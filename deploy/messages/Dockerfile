FROM python:3-alpine

ENV APP_DIR /app
ENV TZ=Europe/Berlin

RUN mkdir -p ${APP_DIR}

COPY message_processor/requirements.txt ${APP_DIR}/

RUN apk add --no-cache --virtual \
  build-dependencies \
  gcc \
  git \
  linux-headers \
  musl-dev \
  python3-dev \
  tzdata \
  && cp /usr/share/zoneinfo/$TZ /etc/localtime \
  && echo $TZ > /etc/timezone \
  && pip3 install -r ${APP_DIR}/requirements.txt \
  && apk del build-dependencies \
  && rm -rf /var/lib/apt/lists/*

COPY message_processor/process_messages.py ${APP_DIR}/
COPY message_processor/messages ${APP_DIR}/messages

CMD python3 ${APP_DIR}/process_messages.py
