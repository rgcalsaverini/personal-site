import json
import os
import random
import time
from email.utils import parseaddr as parse_email
from string import ascii_letters, digits

from flask import Blueprint, session as f_session
from flask_kit import Router, make_error
from app_server.utils import add_message


def random_file_path(path):
    characters = ascii_letters + digits
    name = ''.join([random.choice(characters) for _ in range(18)])
    return os.path.join(path, name)


def create_ui_api(configs, redis, session=f_session):
    blueprint = Blueprint('ui-api', __name__, url_prefix='/ui-api')
    router = Router(blueprint)

    message_schema = {
        'message': {
            'type': 'string',
        },
        'email': {
            'type': 'string',
        },
    }

    @router.post('/messages', validate=message_schema)
    def register_message(data):
        """ Register a contact message """
        message = str(data.get('message', '')).strip()
        email = parse_email(str(data.get('email', '')))[1].strip()

        if not email or '@' not in email or '.' not in email:
            return make_error({'t_email': 'error.invalidEmail'})
        if not message:
            return make_error({'t_message': 'error.emptyMessage'})

        msg_data = json.dumps({
            'message': message,
            'from': email,
            'timestamp': time.time(),
        })

        add_message(redis, configs.messages, msg_data)

        return {'success': True}

    return blueprint
