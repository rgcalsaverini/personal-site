from redis import StrictRedis


def create_redis(rconf, redis_cls=StrictRedis):
    """ Redis factory """
    redis = redis_cls(host=rconf.host, port=rconf.port, db=rconf.db)
    return redis


def add_message(redis, conf, message):
    redis.sadd(conf.rkey_queue, message)
