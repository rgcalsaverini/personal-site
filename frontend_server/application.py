from flask_kit import create_app, get_configs

from app_server import create_ui_api, create_redis

configs = get_configs()
redis = create_redis(configs.redis)

# https is handled by nginx's configs
app = create_app(configs, https=False, blueprints=[
    create_ui_api(configs, redis)
])

if __name__ == '__main__':
    app.run(debug=True)
