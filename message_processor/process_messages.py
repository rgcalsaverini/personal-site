from flask_kit import get_configs

from messages import MessageProcessor

configs = get_configs()
pool = MessageProcessor(configs)
pool.run()
