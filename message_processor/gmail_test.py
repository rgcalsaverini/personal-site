from flask_kit import get_configs

from messages.gmail import build_service, create_message, send_message

configs = get_configs()
service = build_service(configs, can_auth=True)
message = create_message(configs.gmail, {
    'message': 'This is intended to test the mail sending functions.',
    'from': 'test@testme.test',
})
res = send_message(service, configs.gmail.addr_sender, message)
print(res)
