import json
import logging
import signal
import sys
import time

from redis import StrictRedis

from .gmail import create_message, send_message, build_service


class MessageProcessor(object):
    def __init__(self, configs, debug=False):
        self._keep_running = True
        signal.signal(signal.SIGINT, self._exit_gracefully)
        signal.signal(signal.SIGTERM, self._exit_gracefully)
        self.service = build_service(configs, False)
        self.configs = configs
        self.redis = StrictRedis(host=configs.redis.host,
                                 port=configs.redis.port,
                                 db=configs.redis.db)
        self.debug = debug
        self.logger = self._create_logger()

    def _create_logger(self):
        root = logging.getLogger('messages')
        level = logging.DEBUG if self.debug else logging.INFO
        root.setLevel(level)
        log_to = self.configs.messages.logto
        log_outputs = [log_to] if isinstance(log_to, str) else log_to

        for output in log_outputs:
            if output.strip().lower() == 'stdout':
                handler = logging.StreamHandler(sys.stdout)
            elif output.strip().lower() == 'stderr':
                handler = logging.StreamHandler(sys.stderr)
            else:
                handler = logging.FileHandler(output)
            handler.setLevel(level)
            root.addHandler(handler)

        return root

    def _exit_gracefully(self, *_):
        self.logger.info('Exiting gracefully')
        self._keep_running = False

    def run(self):
        try:
            while self._keep_running:
                res = self._process_one()
                if not res:
                    time.sleep(2)
        except KeyboardInterrupt:
            return

    def _process_one(self):
        orig_message = self._get_next_message()

        if not orig_message:
            return False
        self.logger.debug('Got %s' % str(orig_message))
        try:
            message = create_message(self.configs.gmail, orig_message)
            addr_sender = self.configs.gmail.addr_sender
            res = send_message(self.service, addr_sender, message)
        except Exception:
            self.logger.error('Exception while sending %s' % str(orig_message))
            self._rollback(orig_message)
            return True
        if not res or not isinstance(res, dict) or not res.get('id', False):
            self.logger.error('Error while sending %s' % str(orig_message))
            self._rollback(orig_message)
        else:
            self.logger.info('Sent message %s' % str(orig_message))
        return True

    def _get_next_message(self):
        msg_key = self.configs.messages.rkey_queue
        pop_result = self.redis.spop(msg_key, 1)
        if isinstance(pop_result, list) and len(pop_result) > 0:
            msg = pop_result[0].decode('utf-8')
        elif isinstance(pop_result, bytes) and len(pop_result) > 0:
            msg = pop_result.decode('utf-8')
        else:
            return None

        decoded_msg = json.loads(msg)
        if not decoded_msg:
            return None
        return decoded_msg

    def _rollback(self, message):
        msg_key = self.configs.messages.rkey_queue
        self.redis.sadd(msg_key, message)