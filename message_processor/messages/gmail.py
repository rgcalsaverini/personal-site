import base64
import os
import pickle
from email.mime.text import MIMEText

from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build

scopes = [
    'https://www.googleapis.com/auth/gmail.send	',
]

template = """
-------------------------------
ADDR: {from_addr}
IP: {ip}
TIMESTAMP: {timestamp}
-------------------------------

{body}"""


def build_service(configs, can_auth):
    creds = None
    token_path = configs.gmail.token_path

    if os.path.exists(token_path):
        with open(token_path, 'rb') as token:
            creds = pickle.load(token)

    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        elif can_auth:
            flow = InstalledAppFlow.from_client_secrets_file(
                configs.gmail.credentials_path,
                configs.gmail.scopes
            )
            creds = flow.run_local_server()
        else:
            raise RuntimeError('Not authorized')

        with open(token_path, 'wb') as token:
            pickle.dump(creds, token)

    return build('gmail', 'v1', credentials=creds)


def format_message(gmail_configs, data):
    return template.format(
        from_addr=data.get('from', 'empty'),
        ip=gmail_configs.get('ip', 'N/A'),
        timestamp=data.get('timestamp', 'N/A'),
        body=data.get('message', 'empty')
    )


def create_message(gmail_configs, data):
    message = format_message(gmail_configs, data)
    actual_sender = data.get('from', 'empty')
    message = MIMEText(message)
    message['to'] = gmail_configs.addr_to
    message['from'] = gmail_configs.addr_sender
    message['subject'] = '{} - {}'.format(gmail_configs.subject, actual_sender)
    return {
        'raw': base64.urlsafe_b64encode(message.as_bytes()).decode('utf-8')
    }


def send_message(service, user_id, message):
    try:
        messages_service = service.users().messages()
        message = messages_service.send(userId=user_id, body=message).execute()
        return message
    except IOError:
        pass
    return None
