import { connect } from 'react-redux';
import Headers from './Headers';

const stateToProps = state => ({
  langUpdateCount: state.i18nReducer.langUpdateCount,
});

const dispatchToProps = {
};

const ConnectedComponent = connect(
  stateToProps,
  dispatchToProps,
)(Headers);

export default ConnectedComponent;
