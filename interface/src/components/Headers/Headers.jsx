import React from 'react';
import PropTypes from 'prop-types';
import i18n from 'i18next';
import { Helmet } from 'react-helmet';
import { locUrl } from 'app_utils';
import constants, { locales } from 'app_constants';
import { t } from 'app_i18n';

const fallbackDescr = 'A software developer turned manager living in Germany with over 10 years of experience. Passionate about web products, security and technology in general';

const Headers = ({ path, root }) => {
  const rootHeaders = [
    <meta name="description" content={t('meta.description', fallbackDescr)} />,
    <meta property="og:title" content="Rui Geraldes Calsaverini" />,
    <meta property="og:description" content={t('meta.description', fallbackDescr)} />,
    <meta property="og:type" content="website" />,
    <meta property="og:image" content={`${constants.host.replace(/\/+$/g, '')}/imgs/avatar_dark_600px.jpg`} />,
    <meta property="og:url" content={locUrl(path, true)} />,
    <meta property="og:locale" content={i18n.language} />,
  ];
  return (
    <Helmet>
      <html lang={i18n.language} />
      <link rel="canonical" href={locUrl(path, true)} />
      {root && rootHeaders}
      {root && Object.keys(locales).map(loc => (
        <link
          key={loc}
          rel="alternate"
          hrefLang={loc}
          href={locUrl('/', true, loc)}
        />
      ))}
    </Helmet>
  );
};

Headers.propTypes = {
  path: PropTypes.string,
  root: PropTypes.bool,
};

Headers.defaultProps = {
  path: '/',
  root: false,
};

export default Headers;
