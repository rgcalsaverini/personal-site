import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import { Link as RouterLink } from 'react-router-dom';
import {
  flex,
  onMobile,
  onDesktop,
  spacing,
  palette,
} from 'theme';


export const ButtonContainer = styled.span`
  background-color: rgba(255, 255, 255, .7);
`;

export const FlagContainer = styled.div`
  ${flex({ direction: 'row', align: 'center', justify: 'center' })}
  ${onMobile`padding-right: 0px;`}
  ${onDesktop`padding-right: 8px;`}
  position: relative;
  align-self: center;
`;

export const Flag = styled.div`
  background-image: url(${({ image }) => image});
  background-repeat: no-repeat;
  height: ${props => (props.big ? 21 : 16)}px;
  position: ${props => (props.overlay ? 'absolute' : 'relative')};
  width: ${props => (props.big ? 28 : 20)}px;
`;

export const LanguageLabel = styled.div`
  ${onMobile`display: none;`}
`;

export const FlagButton = styled(Button)`
  ${onMobile`background-color: transparent !important;`}
  ${onDesktop`background-color: rgba(255, 255, 255, 0.7) !important;`}
  min-width: 0px !important;
`;

export const Link = styled(RouterLink)`
  ${flex({ direction: 'row', justify: 'flex-start' })}
  text-decoration: none !important;
  min-width: 220px;
  padding: ${spacing.normal / 2}px ${spacing.normal}px;
  :hover {
    background-color: rgba(0,0,0,.1) !important;
  }
`;

export const LangItemText = styled.div`
  text-decoration: none !important;
  color: ${palette.darkText};
  padding-left: ${spacing.normal}px;
  font-size: 18px;
`;
