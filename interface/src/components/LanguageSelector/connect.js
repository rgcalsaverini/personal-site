import { connect } from 'react-redux';
import { setDialogOpen } from 'state_management/actions/i18n';
import LanguageSelector from './LanguageSelector';


const stateToProps = state => ({
  selected: state.i18nReducer.selected,
  dialogOpen: state.i18nReducer.dialogOpen,
  mobile: state.sideMenuReducer.mobile,
});

const dispatchToProps = {
  setDialogOpen,
};

const ConnectedComponent = connect(
  stateToProps,
  dispatchToProps,
)(LanguageSelector);

export default ConnectedComponent;
