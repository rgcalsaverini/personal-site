import React from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import List from '@material-ui/core/List';
import { locales } from 'app_constants';
import { t } from 'app_i18n';
import {
  Flag,
  FlagContainer,
  FlagButton,
  LanguageLabel,
  LangItemText,
  Link,
} from './styles';

const FlagOverlayButton = ({ locId, big }) => {
  const selectedId1 = locId || '';
  const selectedId2 = selectedId1.split('-')[0].split('_')[0];
  return (
    <FlagContainer big={big}>
      <Flag key={1} image={`/imgs/flag_${selectedId2}.svg`} big={big} />
      <Flag key={3} image={`/imgs/flag_${selectedId1}.svg`} overlay big={big} />
    </FlagContainer>
  );
};

FlagOverlayButton.propTypes = {
  big: PropTypes.bool,
  locId: PropTypes.string,
};

FlagOverlayButton.defaultProps = {
  big: false,
  locId: null,
};

const LanguageSelector = (props) => {
  const {
    dialogOpen,
    selected,
    setDialogOpen,
  } = props;
  const selectedName = locales[selected] || 'English';

  return (
    <div>
      <FlagButton name={t('gl.chooseLanguage')} onClick={() => setDialogOpen(true)}>
        <FlagOverlayButton locId={selected} />
        <LanguageLabel>{selectedName}</LanguageLabel>
      </FlagButton>

      <Dialog
        onClose={() => setDialogOpen(false)}
        open={dialogOpen}
      >
        <DialogTitle>{t('gl.chooseLanguage')}</DialogTitle>
        <List>
          {Object.keys(locales).sort().map(loc => (
            <Link to={`/${loc}`} key={loc}>
              <FlagOverlayButton locId={loc} big />
              <LangItemText>{locales[loc]}</LangItemText>
            </Link>
          ))}
        </List>
      </Dialog>
    </div>
  );
};

LanguageSelector.propTypes = {
  dialogOpen: PropTypes.bool.isRequired,
  selected: PropTypes.string,
  setDialogOpen: PropTypes.func.isRequired,
};

LanguageSelector.defaultProps = {
  selected: undefined,
};

export default LanguageSelector;
