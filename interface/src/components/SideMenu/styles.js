import styled from 'styled-components';
import {
  flex,
  spacing,
  transition,
  palette,
  onMobile,
  onDesktop,
  onShortMobile,
} from 'theme';

import constants from 'app_constants';

export const Container = styled.div`
  ${flex({ direction: 'column', justify: 'space-between' })}
  position: fixed;
  transition: ${transition('all', ['box-shadow', 'fast'])};

  ${({ collapsed }) => (onMobile`
    height: 50vh;
    width: 100%;
    z-index: 99;
    overflow: hidden;
    background-color: ${palette.pageBg};
    ${collapsed && `
      background-color: ${palette.accentBg}
      margin-top: calc(-50vh + ${constants.appBarHeight}px);
      box-shadow: 0px 3px 12px 0px rgba(0,0,0,0.4);
    `}
  `)}

  ${({ collapsed }) => (onDesktop`
    height: 100%;
    left: 0px;
    top: 0px;
    width: 50%;
    ${collapsed && `
      border-radius: 6px;
      box-shadow: 2px 2px 8px 0px rgba(0,0,0,0.4);
      height: auto;
      left: ${spacing.normal}px;
      top: ${spacing.normal}px;
      width: ${constants.sideMenuWidth}px;
    `}
  `)}
`;

export const Title = styled.div`
  box-sizing: border-box;
  flex-grow: 1;
  transition: ${transition()};
  color: ${palette.accent};
  padding: ${spacing.normal}px;
  text-align: center;

  ${onMobile`font-size: 4.3vw;`}
  ${onDesktop`
    font-size: 1.4vw;
    ${({ collapsed }) => collapsed && `
      height: 0px;
      max-height: 0px;
      overflow: hidden;
      opacity: 0;
      padding: 0px;
    `}
  `}
  ${onShortMobile`
    padding: 2px;
    font-size: 4vw;
  `}
`;

export const Buttons = styled.div`
${flex({ justify: 'center', align: 'center' })}
  box-sizing: border-box;
  flex-grow: 1;
  transition: ${transition()};
  width: 100%;

  ${onMobile`${({ collapsed }) => collapsed && `
    height: ${constants.appBarHeight}px;
    max-height: ${constants.appBarHeight}px;
  `}`}

  ${onDesktop`${({ collapsed }) => collapsed && `
    height: 0px;
    max-height: 0px;
    overflow: hidden;
    opacity: 0;
  `}`}

`;
