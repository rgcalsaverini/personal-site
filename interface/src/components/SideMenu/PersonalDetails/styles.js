import styled from 'styled-components';
import {
  flex,
  spacing,
  transition,
  fonts,
  palette,
  onMobile,
  onDesktop,
  onShortMobile,
} from 'theme';

const firstWidth = mobile => (mobile ? 15 : 8);
const lastWidth = mobile => firstWidth(mobile) * 0.329375;

export const Container = styled.div`
  width: 100%;
  ${onMobile([flex({ direction: 'row', align: 'center' })])}
  ${onDesktop([flex({ direction: 'column', align: 'center' })])}
  ${onDesktop`${({ collapsed }) => `
    justify-content: ${collapsed ? 'flex-start' : 'space-around'};
    flex-grow: ${collapsed ? 0 : 1};
  `}`}
`;

export const Name = styled.h1`
  ${flex({
    direction: 'column',
    justify: 'space-around',
    align: 'center',
  })}
  font-family: ${fonts.special};
  width: 100%;
  color: ${palette.darkText};
  margin: 0px;
  padding: 0px;
`;


export const FirstName = styled.div`
  letter-spacing: 2vw;
  text-indent: 2vw;
  transition: ${transition()};
  ${onMobile`
    font-size: ${firstWidth(true)}vw;
  `}
  ${onDesktop`
    font-size: ${({ collapsed }) => (collapsed ? 0 : firstWidth(false))}vw;
  `}
  ${onShortMobile`
    font-size: ${firstWidth(true)}vw;
  `}
}`;

export const LastName = styled.div`
text-transform: uppercase;
transition: ${transition()};

${onMobile`
  font-size: ${lastWidth(true)}vw;
`}
${onDesktop`
  font-size: ${({ collapsed }) => (collapsed ? 0 : lastWidth(false))}vw;
`}
${onShortMobile`
  font-size: ${lastWidth(true)}vw;
`}

  ${({ collapsed }) => (`
    color: ${collapsed ? palette.lightText : palette.darkText};
    letter-spacing: ${collapsed ? '-1px' : '-0.15vw'};
    text-indent: ${collapsed ? '-1px' : '-0.15vw'};
    margin-top: ${collapsed ? '-50px' : '0px'};
`)}`;

export const Avatar = styled.img`
  transition: ${transition()};
  ${onDesktop`${({ collapsed }) => `
    cursor: ${collapsed ? 'pointer' : 'auto'};
    max-width: ${collapsed ? 100 : 40}%;
    padding: ${collapsed ? 0 : spacing.large}px;
    border-top-left-radius: ${collapsed ? `${6}px` : `${50}%`};
    border-top-right-radius: ${collapsed ? `${6}px` : `${50}%`};
    border-bottom-left-radius: ${collapsed ? 0 : 50}%;
    border-bottom-right-radius: ${collapsed ? 0 : 50}%;

  `}`}

  ${onDesktop`
  ${({ collapsed }) => collapsed && `
    :hover {
      opacity: 0.9;
    }
  `}`}

  ${onMobile`
    max-width: 30%;
    border-radius: 50%;
    padding: ${spacing.normal}px;
  `}

  ${onShortMobile`
    max-width: 23vh;
    padding: ${spacing.normal}px ${spacing.large}px;
  `}
`;
