import React from 'react';
import PropTypes from 'prop-types';
import {
  Container,
  Name,
  Avatar,
  FirstName,
  LastName,
} from './styles';

const PersonalDetails = (props) => {
  const {
    mobile,
    collapsed,
    toTop,
  } = props;

  return (
    <Container collapsed={collapsed}>
      <Avatar
        alt="me"
        collapsed={collapsed}
        onClick={(collapsed && !mobile) ? toTop : undefined}
        src="/imgs/avatar_dark_150px.jpg"
        srcSet="/imgs/avatar_dark_150px.jpg 150w,
                /imgs/avatar_dark_300px.jpg 300w,
                /imgs/avatar_dark_600px.jpg 600w "
        sizes="(max-width: 600px) 150px,
               (max-width: 1200px) 300px,
               600px"
      />
      <Name itemProp="name" content="Rui Calsaverini">
        <FirstName
          collapsed={collapsed}
          itemProp="givenName"
          content="Rui"
        >
          RUI
        </FirstName>
        <LastName
          collapsed={collapsed}
          itemProp="familyName"
          content="Calsaverini"
        >
          CALSAVERINI
        </LastName>
      </Name>
    </Container>
  );
};

PersonalDetails.propTypes = {
  mobile: PropTypes.bool.isRequired,
  collapsed: PropTypes.bool.isRequired,
  toTop: PropTypes.func.isRequired,
};

PersonalDetails.defaultProps = {
};

export default PersonalDetails;
