import { connect } from 'react-redux';
import { navigateToSection } from 'state_management/actions/side_menu';
import SideMenu from './SideMenu';


const stateToProps = state => ({
  mobile: state.sideMenuReducer.mobile,
  collapsed: state.sideMenuReducer.collapsed,
  sections: state.sideMenuReducer.sections,
  activeSection: state.sideMenuReducer.activeSection,
  langUpdateCount: state.i18nReducer.langUpdateCount,
});

const dispatchToProps = {
  navigateToSection,
};

const ConnectedComponent = connect(
  stateToProps,
  dispatchToProps,
)(SideMenu);

export default ConnectedComponent;
