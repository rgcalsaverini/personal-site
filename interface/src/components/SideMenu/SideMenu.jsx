import React from 'react';
import PropTypes from 'prop-types';
import ButtonBox from 'components/ButtonBox';
import { t } from 'app_i18n';

import {
  Container,
  Title,
  Buttons,
} from './styles';
import PersonalDetails from './PersonalDetails';
import SectionMenu from './SectionMenu';

const SideMenu = (props) => {
  const {
    mobile,
    collapsed,
    sections,
    activeSection,
    navigateToSection,
  } = props;

  const title = `${t('titles.seniorDev')} & ${t('titles.projectManager')}`;

  return (
    <Container
      collapsed={collapsed}
      itemType="http://schema.org/Person"
      itemScope
    >
      <PersonalDetails
        mobile={mobile}
        collapsed={collapsed}
        toTop={() => navigateToSection(0)}
        itemProp="gender"
        content="male"
      />
      <Title
        itemProp="jobTitle"
        collapsed={collapsed}
      >
        {title}
      </Title>
      <Buttons collapsed={collapsed}>
        <ButtonBox
          collapsed={collapsed}
          mobile={mobile}
          t={t}
        />
      </Buttons>
      <SectionMenu
        onSelect={navigateToSection}
        activeSection={activeSection}
        sections={sections}
        show={collapsed}
        t={t}
      />
    </Container>
  );
};

SideMenu.propTypes = {
  collapsed: PropTypes.bool.isRequired,
  mobile: PropTypes.bool.isRequired,
  sections: PropTypes.instanceOf(Map).isRequired,
  activeSection: PropTypes.string,
  navigateToSection: PropTypes.func.isRequired,
};

SideMenu.defaultProps = {
  activeSection: null,
};

export default SideMenu;
