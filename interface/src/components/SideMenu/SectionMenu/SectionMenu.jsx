import React from 'react';
import PropTypes from 'prop-types';
import { htmlBool } from 'app_utils';
import { t } from 'app_i18n';import {
  Container,
  Section,
} from './styles';

const SectionMenu = (props) => {
  const {
    show,
    sections,
    activeSection,
    onSelect,
  } = props;

  const sectionKeys = Array.from(sections.keys());

  return (
    <Container show={show}>
      {sectionKeys.map((sec, i) => {
        const isActive = activeSection === sec;
        return (
          <Section
            key={sec}
            last={htmlBool(i === sectionKeys.length - 1)}
            active={htmlBool(isActive)}
            disabled={isActive}
            onClick={() => onSelect(sec)}
          >
            {t(`menu.${sec}`)}
          </Section>
        );
      })}
    </Container>
  );
};

SectionMenu.propTypes = {
  show: PropTypes.bool.isRequired,
  sections: PropTypes.instanceOf(Map).isRequired,
  activeSection: PropTypes.string,
  onSelect: PropTypes.func.isRequired,
};

SectionMenu.defaultProps = {
  activeSection: null,
};

export default SectionMenu;
