import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import {
  transition,
  palette,
  spacing,
  onMobile,
} from 'theme';

export const Container = styled.div`
  overflow: hidden;
  transition: ${transition()};
  ${onMobile`display: none;`}
  ${({ show }) => `
    height: ${show ? 'auto' : '0px'}
    max-height: ${show ? 'auto' : '0px'}
    opacity: ${show ? 1 : 0}
    overflow: ${show ? 'visible' : 'hidden'};
  `}
  border-bottom-left-radius: 8px;
  border-bottom-right-radius: 8px;
  background-color: ${palette.accentBg};
`;

export const Section = styled(Button)`
  border-top-left-radius: 0px !important;
  border-top-right-radius: 0px !important;
  border-bottom-left-radius: ${({ last }) => (last ? '6px' : '0px')} !important;
  border-bottom-right-radius: ${({ last }) => (last ? '6px' : '0px')} !important;

  ${({ active }) => `
    background-color: ${active ? 'rgba(255, 255, 255, .85)' : 'transparent'} !important;
    color: ${active ? palette.darkText : palette.lightText} !important;
  `}
  width: 100% !important;
  padding: ${spacing.normal}px 0px ${spacing.normal}px 0px !important;

  : hover {
    color: ${palette.accent} !important;
  }
`;
