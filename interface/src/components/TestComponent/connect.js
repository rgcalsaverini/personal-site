import { connect } from 'react-redux';
import { setCollapsed } from 'state_management/actions/side_menu';
import * as sideMenuActions from 'state_management/actions/side_menu';
import * as contactActions from 'state_management/actions/contact';
import * as i18nActions from 'state_management/actions/i18n';
import TestComponent from './TestComponent';


const stateToProps = state => ({
  state,
  contactActions,
  i18nActions,
  sideMenuActions,
});

const dispatchToProps = {
  setCollapsed,
};

const ConnectedComponent = connect(
  stateToProps,
  dispatchToProps,
)(TestComponent);

export default ConnectedComponent;
