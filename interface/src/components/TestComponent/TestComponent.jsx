import React, { Component } from 'react';
import styled from 'styled-components';
import MUIButton from '@material-ui/core/Button';
import { getWidth } from 'app_utils';

const Container = styled.div`
  box-sizing: border-box;
  background-color: ${({ open }) => (open ? '#F0F0F0' : '#FBB')};
  border: 1px dashed #888;
  position: fixed;
  bottom: ${({ open }) => (open ? '5px' : '-300px')};
  right: ${({ open }) => (open ? '5px' : '-230px')};
  width: 250px;
  z-index: 9999999;
  transition: all ease-in-out 100ms;
  padding: ${({ open }) => (open ? '12px' : '50px')}; 12px 12px 12px;
  height: 320px;
  overflow: scroll;
  border-radius: 10px;
`;

const StateContainer = styled.pre`
  font-size: 8px;
`;

const ActionContainer = styled.div`
  width: 100%;
  display: flex;
  font-size: 12px;
`;

const Button = styled(MUIButton)`
  font-size: 10px !important;
  padding: 1px !important;
  min-width: 0px !important;
  border: 1px solid black !important;
  margin-right: 5px !important;
  text-transform: none !important;
`;
const DataInput = styled.input`
  width: 100%;
  background-color: ${({ error }) => (error ? '#FDD' : 'white')};
`;

const getBodyScroll = () => {
  const offset = (window.pageYOffset || document.scrollTop);
  const top = (document.clientTop || 0);
  const scroll = offset - top;
  return Number.isNaN(scroll) ? 0 : scroll;
};

class TestComponent extends Component {
  state = {
    open: false,
    actionData: [],
    actionDataError: false,
  }
  render() {
    const {
      state,
      contactActions,
      i18nActions,
      sideMenuActions,
    } = this.props;

    const allActions = {
      'contact': contactActions,
      'i18n': i18nActions,
      'side_menu': sideMenuActions,
    };

    if (! this.state.open) {
      return (<Container onClick={() => this.setState({ open: true })} />)
    }

    const visibleReachedSections = Array.from(state.sideMenuReducer.reachedSections).join(',');
    const visibleSectionOffsets = Array.from(state.sideMenuReducer.sectionOffsets).map((i) => (
      `[${i.join(',')}]`
    ));
    const visibleSections = Array.from(state.sideMenuReducer.sections).map((i) => (
      `[${i.join(',')}]`
    ));
    const printableState = JSON.parse(JSON.stringify(state));
    printableState.sideMenuReducer.sections = `Map(${visibleSections})`;
    printableState.sideMenuReducer.sectionOffsets = `Map(${visibleSectionOffsets})`;
    printableState.sideMenuReducer.reachedSections = `Set(${visibleReachedSections})`;

    return (
      <Container open>
        <Button onClick={() => this.setState({ open: false })}>
          CLOSE
        </Button>
        <StateContainer>
          {`width = ${getWidth()}\n`}
          {`window.pageYOffset = ${window.pageYOffset}\n`}
          {`document.scrollTop = ${document.scrollTop}\n`}
          {`document.clientTop = ${document.clientTop}\n`}
          {`bodyScroll = ${getBodyScroll()}\n`}
          {`state = ${JSON.stringify(printableState, undefined, 4)}`}
        </StateContainer>
        <DataInput
          error={this.state.actionDataError ? 'error' : undefined}
          onChange={(v) => {
            let data = [];
            try {
              data = JSON.parse(`[${v.target.value}]`);
            }
            catch(e) {
              this.setState({ actionDataError: true});
              return;
            }
            this.setState({ actionData: data, actionDataError: false});
          }}
          type="text"
        />
      {Object.keys(allActions).map((category) => (
        Object.keys(allActions[category]).map((action) =>(
          <ActionContainer key={`${category}.${action}`}>
            <Button
              onClick={() => {
                window.store.dispatch(allActions[category][action](...this.state.actionData));
                console.log(`Dispatched action\n${allActions[category][action]}`);
              } }
            >
            {category}.{action}
          </Button>
          </ActionContainer>
        ))
      ))}

      </Container>
    );
  }
}

export default TestComponent;
