import { connect } from 'react-redux';
import App from './App';

const stateToProps = state => ({
  collapsed: state.sideMenuReducer.collapsed,
  language: state.i18nReducer.language,
});

const dispatchToProps = {
};

const ConnectedComponent = connect(
  stateToProps,
  dispatchToProps,
)(App);

export default ConnectedComponent;
