import React from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';
import SideMenu from 'components/SideMenu';
import LanguageSelector from 'components/LanguageSelector';
import MobileDrawer from 'components/MobileDrawer';
import TestComponent from 'components/TestComponent';
import Headers from 'components/Headers';
import constants from 'app_constants';
import {
  Intro,
  Management,
  Others,
  Security,
  WebDev,
} from 'components/Pages';
import LanguageSwitch from 'components/LanguageSwitch';

import { Container, Contents, LanguageContainer } from './styles';

const App = (props) => {
  const { collapsed } = props;
  return (
    <Container>
      <Headers path="/" root />
      <SideMenu />

      <Contents collapsed={collapsed}>
        <Intro />
        <WebDev />
        <Security />
        <Management />
        <Others />
      </Contents>

      <LanguageContainer>
        <LanguageSelector />
      </LanguageContainer>

      <MobileDrawer />

      <Route component={LanguageSwitch} />
      {constants.isDev && <TestComponent />}
    </Container>
  );
};

App.propTypes = {
  collapsed: PropTypes.bool.isRequired,
};

App.defaultProps = {
};

export default App;
