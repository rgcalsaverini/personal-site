import styled from 'styled-components';
import {
  flex,
  transition,
  spacing,
  onMobile,
  onDesktop,
} from 'theme';
import constants from 'app_constants';

export const Container = styled.div`
  ${onMobile([flex({ direction: 'column' })])}
  ${onDesktop([flex({ direction: 'row' })])}
  min-height: 100vh;
  position: static;
  width: 100%;
`;

export const Contents = styled.div`
transition: ${transition()};
position: static;

  ${({ collapsed }) => (onMobile`
    min-height: 50vh;
    width: 100%;
    margin-top: ${collapsed ? `${constants.appBarHeight + 5}px` : `${50}vh`};
  `)}
  ${({ collapsed }) => (onDesktop`
    box-sizing: border-box;
    width: ${collapsed ? 'auto' : '50%'};
    min-height: 100vh;
    height: 100%;
    margin-left: ${collapsed ? `${constants.sideMenuWidth + spacing.normal * 2}px` : `${50}%`};
  `)}
`;

export const LanguageContainer = styled.div`
  position: fixed;
  right: ${spacing.normal}px;
  top: ${spacing.normal}px;
  ${onMobile`display: none;`}
`;
