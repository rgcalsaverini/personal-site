import { connect } from 'react-redux';
import {
  setMessage,
  setEmail,
  sendAnotherMessage,
  sendMessage,
  clearContactDialog,
} from 'state_management/actions/contact';
import ContactDialog from './ContactDialog';


const stateToProps = state => ({
  email: state.contactReducer.email,
  emailError: state.contactReducer.emailError,
  langUpdateCount: state.i18nReducer.langUpdateCount,
  message: state.contactReducer.message,
  messageError: state.contactReducer.messageError,
  messageSent: state.contactReducer.messageSent,
  mobile: state.sideMenuReducer.mobile,
  pendingResponse: state.contactReducer.pendingResponse,
});

const dispatchToProps = {
  clearContactDialog,
  sendAnotherMessage,
  sendMessage,
  setEmail,
  setMessage,
};

const ConnectedComponent = connect(
  stateToProps,
  dispatchToProps,
)(ContactDialog);

export default ConnectedComponent;
