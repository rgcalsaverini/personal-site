import React from 'react';
import PropTypes from 'prop-types';
import RouterPropTypes from 'react-router-prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import Icon from '@material-ui/core/Icon';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';
import { locUrl } from 'app_utils';
import { t } from 'app_i18n';

import {
  Button,
  Container,
  InternalContainer,
  Contacts,
  Contact,
  ContactText,
  TextHint,
  TextBig,
  Message,
  ButtonContainer,
  MessageSent,
} from './styles';

const obfusEmail = [351, 357, 333, 251, 321, 317, 339, 353, 317,
  359, 325, 351, 333, 343, 333, 215, 321, 345, 341];

const obfusPhone = [209, 227, 237, 187, 221, 229, 223, 219, 187, 231, 225, 223, 237, 229, 223, 233];

const deobfuscate = str => str.map(c => String.fromCharCode((c - 123) / 2)).join('');

const closeDialog = (history, clear) => {
  history.push(locUrl('/'));
  clear();
};

const ContactDialog = (props) => {
  const {
    clearContactDialog,
    email,
    emailError,
    history,
    message,
    messageError,
    messageSent,
    mobile,
    pendingResponse,
    sendAnotherMessage,
    sendMessage,
    setEmail,
    setMessage,
  } = props;

  return (
    <Container>
      <Dialog
        onClose={() => closeDialog(history, clearContactDialog)}
        open
        scroll="paper"
        fullScreen={mobile}
      >
        <DialogTitle>{t('menu.Contact', { context: 'me' })}</DialogTitle>
        <InternalContainer>
          <Contacts>
            <Contact
              href={`mailto:${deobfuscate(obfusEmail)}`}
            >
              <Icon color="primary" fontSize="large"> mail </Icon>
              <ContactText>
                <TextHint>{t('gl.email')}</TextHint>
                <TextBig onClick={event => event.stopPropagation()}>
                  {deobfuscate(obfusEmail)}
                </TextBig>
              </ContactText>
            </Contact>
            <Contact
              href={`tel:${deobfuscate(obfusPhone).replace(' ', '')}`}
            >
              <Icon color="primary" fontSize="large"> phone </Icon>
              <ContactText>
                <TextHint>{t('gl.phone')}</TextHint>
                <TextBig onClick={event => event.stopPropagation()}>
                  {deobfuscate(obfusPhone)}
                </TextBig>
              </ContactText>
            </Contact>
          </Contacts>
          <Message>
            <div>{`...${t('text.justWrite')}`}</div>
            <TextField
              autoComplete="email"
              error={emailError !== false}
              fullWidth
              helperText={(emailError && t(emailError)) || ''}
              label={t('gl.email')}
              margin="normal"
              onChange={({ target }) => setEmail(target.value)}
              type="email"
              value={email || ''}
              variant="outlined"
            />
            <TextField
              fullWidth
              error={messageError !== false}
              helperText={(messageError && t(messageError)) || ''}
              label={t('gl.yourMessage')}
              margin="normal"
              multiline
              onChange={({ target }) => setMessage(target.value)}
              rows={mobile ? '1' : '8'}
              rowsMax={mobile ? '4' : '8'}
              value={message || ''}
              variant="outlined"
            />
            <ButtonContainer>
              <Button
                disabled={pendingResponse}
                onClick={() => closeDialog(history, clearContactDialog)}
                name={t('gl.close')}
              >
                {t('gl.close')}
              </Button>
              <Button
                variant={messageSent ? undefined : 'contained'}
                color="primary"
                last
                disabled={(pendingResponse || emailError || messageError) && true}
                onClick={() => sendMessage(email, message)}
                name={t('gl.sendMessage')}
              >
                {pendingResponse ? <CircularProgress /> : t('gl.sendMessage')}
              </Button>
            </ButtonContainer>
            {messageSent && (
            <MessageSent>
              <div>{t('text.thanksMsg')}</div>
              <ButtonContainer center>
                <Button
                  name={t('gl.close')}
                  onClick={() => closeDialog(history, clearContactDialog)}
                >
                  {t('gl.close')}
                </Button>
                <Button
                  last
                  color="primary"
                  onClick={() => sendAnotherMessage()}
                  name={t('gl.close')}
                >
                  {t('gl.sendAnotherMessage')}
                </Button>
              </ButtonContainer>
            </MessageSent>
            )}
          </Message>
        </InternalContainer>
      </Dialog>
    </Container>
  );
};

ContactDialog.propTypes = {
  clearContactDialog: PropTypes.func.isRequired,
  email: PropTypes.string,
  emailError: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  history: RouterPropTypes.history.isRequired,
  message: PropTypes.string,
  messageError: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  messageSent: PropTypes.bool.isRequired,
  mobile: PropTypes.bool.isRequired,
  pendingResponse: PropTypes.bool.isRequired,
  sendAnotherMessage: PropTypes.func.isRequired,
  sendMessage: PropTypes.func.isRequired,
  setEmail: PropTypes.func.isRequired,
  setMessage: PropTypes.func.isRequired,
};

ContactDialog.defaultProps = {
  email: undefined,
  emailError: false,
  message: undefined,
  messageError: false,
};

export default ContactDialog;

// href="tel:+4915206329527"
