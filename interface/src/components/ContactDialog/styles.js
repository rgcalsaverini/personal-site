import styled from 'styled-components';
import { flex, spacing, text, expand, transition } from 'theme';
import MUIButton from '@material-ui/core/Button';

export const Container = styled.span`
  position: absolute;
`;

export const InternalContainer = styled.div`
  ${flex({ direction: 'column' })}
  min-width: 30vw;
  max-width: 800px;
  min-height: 430px;
  padding: ${spacing.normal}px;
`;

export const Contacts = styled.div`
  ${flex({ direction: 'row', justify: 'space-around' })}
  flex-wrap: wrap;
`;

export const Contact = styled.a`
  ${flex({ direction: 'row', align: 'center' })}
  min-width: 220px;
  cursor: pointer;
  background-color: white;
  transition: ${transition()}
  padding: ${spacing.normal}px;
  border-radius: 2px;
  text-decoration: none;
  flex-grow: 1;
  :hover {
    background-color: #F2F2F2;
  }
`;

export const ContactText = styled.div`
  ${flex({ direction: 'column', justify: 'center' })}
  margin-left: ${spacing.normal}px;
  cursor: auto;
`;

export const TextHint = styled.div`
  ${flex({ direction: 'column' })}
  ${text({ size: '12px', color: 'accent' })}
  height: 15px;
`;

export const TextBig = styled.div`
  ${flex({ direction: 'column' })}
  ${text({ size: '16px' })}
`;

export const Message = styled.div`
  ${flex({ direction: 'column' })}
  margin-top: ${spacing.large}px;
  position: relative;
`;

export const MessageSent = styled.div`
  ${flex({ direction: 'col', justify: 'center', align: 'center' })}
  ${expand}
  position: absolute;
  z-index: 9999;
  background-color: white;
`;

export const ButtonContainer = styled.div`
  ${flex({ direction: 'row' })}
  flex-shrink: 1;
  align-self: ${props => (props.center ? 'center' : 'flex-end')};
  padding-top: ${props => (props.center ? spacing.normal : 0)}px;
`;

export const Button = styled(MUIButton)`
  margin-right: ${props => (props.last ? '0' : spacing.normal)}px !important;
`;
