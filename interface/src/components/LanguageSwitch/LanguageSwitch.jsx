import React from 'react';
import PropTypes from 'prop-types';
import RouterPropTypes from 'react-router-prop-types';
import { Route, Redirect, Switch } from 'react-router-dom';
import ContactDialog from 'components/ContactDialog';
import i18n from 'i18next';
import { locales } from 'app_constants';
import ComingSoon from './ComingSoon';

const SpecificLanguage = ({ loc }) => {
  i18n.changeLanguage(loc);
  return (
    <Switch>
      <Route exact path={`/${loc}/contact`} component={ContactDialog} />
      <Route exact path={`/${loc}/coming-soon`} component={ComingSoon} />
      <Route exact path={`/${loc}`} />
      <Redirect to={`/${loc}`} />
    </Switch>
  );
};

SpecificLanguage.propTypes = { loc: PropTypes.string.isRequired };

const LanguageSwitch = ({ match }) => {
  const requested = match.params.loc;
  const localesEmpty = Object.keys(locales).length === 0;
  return (
    <Switch>
      {!localesEmpty && !locales[requested] && <Redirect to="/en" />}
      {!localesEmpty && Object.keys(locales).map(loc => (
        <Route
          key={loc}
          path={`/${loc}`}
          render={routeProps => (
            <SpecificLanguage
              {...routeProps}
              loc={loc}
            />
          )}
        />
      ))}
    </Switch>
  );
};

LanguageSwitch.propTypes = { match: RouterPropTypes.match.isRequired };


export default LanguageSwitch;
