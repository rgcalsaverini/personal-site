import { connect } from 'react-redux';
import LanguageSwitch from './LanguageSwitch';

const stateToProps = state => ({
});

const dispatchToProps = {
};

const ConnectedComponent = connect(
  stateToProps,
  dispatchToProps,
)(LanguageSwitch);

export default ConnectedComponent;
