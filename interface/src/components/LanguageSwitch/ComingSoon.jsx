import React from 'react';
import RouterPropTypes from 'react-router-prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import { t } from 'app_i18n';
import styled from 'styled-components';
import { locUrl } from 'app_utils';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 380px;
  max-width: 80vw;
  padding: 20px 10px;
`;

const NoContent = ({ history }) => (
  <Dialog
    open
    onClose={() => history.push(locUrl('/'))}
  >
    <DialogTitle>
      {`${t('comingSoon.sorryTitle')}...`}
    </DialogTitle>
    <DialogContent>
      <Container>
        <p>{`...${t('comingSoon.sorryText1')}`}</p>
        <p>{`${t('comingSoon.sorryText2')}`}</p>
      </Container>
      <Button
        variant="contained"
        color="primary"
        size="large"
        onClick={() => history.push(locUrl('/contact'))}
        name={t('comingSoon.contactMe')}
      >
        {t('comingSoon.contactMe')}
      </Button>
    </DialogContent>
  </Dialog>
);

NoContent.propTypes = {
  history: RouterPropTypes.history.isRequired,
};

export default NoContent;
