import { connect } from 'react-redux';
import { setDrawer, navigateToSection } from 'state_management/actions/side_menu';
import MobileDrawer from './MobileDrawer';


const stateToProps = state => ({
  drawerOpen: state.sideMenuReducer.drawerOpen,
  activeSection: state.sideMenuReducer.activeSection,
  sections: state.sideMenuReducer.sections,
  selectedLanguage: state.i18nReducer.selected,
});

const dispatchToProps = {
  setDrawer,
  navigateToSection,
};

const ConnectedComponent = connect(
  stateToProps,
  dispatchToProps,
)(MobileDrawer);

export default ConnectedComponent;
