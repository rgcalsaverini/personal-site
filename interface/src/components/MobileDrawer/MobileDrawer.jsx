import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Icon from '@material-ui/core/Icon';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { onDesktop } from 'theme';
import { t } from 'app_i18n';

const DrawerContainer = styled.div`
  width: 220px;
  max-width: 80vw;
  ${onDesktop`display: none;`}
`;

const MobileDrawer = (props) => {
  const {
    activeSection,
    drawerOpen,
    navigateToSection,
    sections,
    setDrawer,
  } = props;

  return (
    <Drawer open={drawerOpen} onClose={() => setDrawer(false)}>
      <DrawerContainer>
        <List component="nav">
          <ListItem button>
            <Icon> email </Icon>
            <ListItemText inset color="primary" primary={t('menu.Contact')} />
          </ListItem>
          <ListItem button>
            <Icon> play_for_work </Icon>
            <ListItemText inset color="primary" primary={t('menu.Resume')} />
          </ListItem>
          <ListItem button>
            <Icon> description </Icon>
            <ListItemText inset color="primary" primary={t('menu.Blog')} />
          </ListItem>
          <ListItem button>
            <ListItemText primary="LinkedIn" />
          </ListItem>
          <ListItem button>
            <ListItemText primary="GitHub" />
          </ListItem>
          <Divider />
          {Array.from(sections.keys()).map(sec => (
            <ListItem button key={sec}>
              <ListItemText
                selected={sec === activeSection}
                onClick={() => navigateToSection(sec)}
                primary={t(`menu.${sec}`)}
              />
            </ListItem>
          ))}
        </List>
      </DrawerContainer>
    </Drawer>
  );
};
MobileDrawer.propTypes = {
  activeSection: PropTypes.string,
  drawerOpen: PropTypes.bool.isRequired,
  navigateToSection: PropTypes.func.isRequired,
  setDrawer: PropTypes.func.isRequired,
};

MobileDrawer.defaultProps = {
  activeSection: null,
};

export default MobileDrawer;

// href="tel:+4915206329527"
