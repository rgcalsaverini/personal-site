import styled from 'styled-components';
import MUICard from '@material-ui/core/Card';
import { spacing, flex } from 'theme';
import Typography from '@material-ui/core/Typography';
import MUICardContent from '@material-ui/core/CardContent';

const cardWidth = numCards => `calc(${100.0 / numCards}% - ${2 * spacing.normal}px)`;

export const Card = styled(MUICard)`
  ${flex({ direction: 'column' })}
  box-sizing: border-box !important;
  width: ${({ numCards }) => cardWidth(numCards)} !important;
  max-width: ${({ numCards }) => cardWidth(numCards)} !important;
  flex-grow: 1;
  // max-height: fit-content !important;
  margin: ${spacing.normal}px;
  background-color: #F2F2F2 !important;
`;

export const CardContent = styled(MUICardContent)`
  ${flex({ direction: 'column' })}
  flex-grow: 1;
`;

export const CardMedia = styled.div`
 ${({ image }) => image && `
   background-image: url('${image}');
 `}
  height: 120px;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
`;

export const Chip = styled.span`
  background-color: #DEDEDE;
  font-size: 13px;
  color: rgba(0, 0, 0, .8);
  padding: 3px 6px;
  border-radius: 8px;
  margin: 3px;

  :before {
    content: '#';
    padding-right: 8px;
    font-weight: 800;
    color: rgba(0, 0, 0, .5);
  }
`;

export const ChipContainer = styled.div`
  ${flex({})}
  flex-wrap: wrap;
  padding-bottom: ${spacing.normal}px;
`;

export const Description = styled(Typography)`
  flex-grow: 1;
`;

export const _ = undefined;
