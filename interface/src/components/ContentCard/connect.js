import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import ContentCard from './ContentCard';

const stateToProps = state => ({
  numCards: state.sideMenuReducer.numCards,
  selectedLanguage: state.i18nReducer.selected,
});

const dispatchToProps = {
};

const ConnectedComponent = connect(
  stateToProps,
  dispatchToProps,
)(ContentCard);

export default withRouter(ConnectedComponent);
