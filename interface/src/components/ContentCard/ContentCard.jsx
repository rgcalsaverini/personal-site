import React from 'react';
import PropTypes from 'prop-types';
import RouterPropTypes from 'react-router-prop-types';
import CardActions from '@material-ui/core/CardActions';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { t } from 'app_i18n';
import { locUrl } from 'app_utils';

import {
  Chip,
  Card,
  CardMedia,
  ChipContainer,
  Description,
  CardContent,
} from './styles';

const ContentCard = (props) => {
  const {
    title,
    description,
    image,
    imageTitle,
    contentType,
    actions,
    tags,
    numCards,
    loaded,
    history,
  } = props;

  return (
    <Card numCards={numCards}>
      <CardMedia
        image={loaded && image}
        title={imageTitle}
      />
      <CardContent>
        <Typography color="textSecondary" gutterBottom>
          {contentType || t('gl.personalProject')}
        </Typography>
        <Typography gutterBottom variant="h5" component="h2">
          {title}
        </Typography>
        <div>
          <ChipContainer>
            {tags.map(tag => (
              <Chip key={tag}>
                {t(`tags.${tag}`, tag)}
              </Chip>
            ))}
          </ChipContainer>
        </div>
        <Description component="p">
          {description}
        </Description>
      </CardContent>
      <CardActions>
        {actions.map(([label, callback]) => (
          <Button
            size="small"
            color="primary"
            onClick={callback || (() => history.push(locUrl('/coming-soon')))}
            key={label}
            name={label}
          >
            {label}
          </Button>
        ))}
      </CardActions>
    </Card>
  );
};

ContentCard.propTypes = {
  actions: PropTypes.arrayOf(PropTypes.array),
  articleId: PropTypes.string.isRequired,
  contentType: PropTypes.string,
  description: PropTypes.string.isRequired,
  history: RouterPropTypes.history.isRequired,
  image: PropTypes.string.isRequired,
  imageTitle: PropTypes.string,
  loaded: PropTypes.bool,
  numCards: PropTypes.number.isRequired,
  tags: PropTypes.arrayOf(PropTypes.string),
  title: PropTypes.string.isRequired,
};

ContentCard.defaultProps = {
  imageTitle: undefined,
  actions: [],
  contentType: undefined,
  tags: [],
  loaded: true,
};

export default ContentCard;
