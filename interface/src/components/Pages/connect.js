import { connect } from 'react-redux';
import { addSection, navigateToSection } from 'state_management/actions/side_menu';
import BareIntro from './Intro';
import BareBasePage from './BasePage';
import BareManagement from './Management';
import BareOthers from './Others';
import BareSecurity from './Security';
import BareWebDev from './WebDev';

// import Bare from './';

/* BasePage */
const BasePageStates = state => ({
  sections: state.sideMenuReducer.sections,
  mobile: state.sideMenuReducer.mobile,
  langUpdateCount: state.i18nReducer.langUpdateCount,
});

const BasePageDispatches = {
  addSection,
};

const BasePage = connect(
  BasePageStates,
  BasePageDispatches,
)(BareBasePage);

/* Intro */
const IntroStates = state => ({
  collapsed: state.sideMenuReducer.collapsed,
  mobile: state.sideMenuReducer.mobile,
  langUpdateCount: state.i18nReducer.langUpdateCount,
});

const IntroDispatches = {
  navigateToSection,
};

const Intro = connect(
  IntroStates,
  IntroDispatches,
)(BareIntro);

/* Management */
const ManagementStates = state => ({
  reachedSections: state.sideMenuReducer.reachedSections,
  langUpdateCount: state.i18nReducer.langUpdateCount,
});

const ManagementDispatches = {
};

const Management = connect(
  ManagementStates,
  ManagementDispatches,
)(BareManagement);

/* Others */
const OthersStates = state => ({
  reachedSections: state.sideMenuReducer.reachedSections,
  langUpdateCount: state.i18nReducer.langUpdateCount,
});

const OthersDispatches = {
};

const Others = connect(
  OthersStates,
  OthersDispatches,
)(BareOthers);

/* Security */
const SecurityStates = state => ({
  reachedSections: state.sideMenuReducer.reachedSections,
  langUpdateCount: state.i18nReducer.langUpdateCount,
});

const SecurityDispatches = {
};

const Security = connect(
  SecurityStates,
  SecurityDispatches,
)(BareSecurity);

/* WebDev */
const WebDevStates = state => ({
  reachedSections: state.sideMenuReducer.reachedSections,
  langUpdateCount: state.i18nReducer.langUpdateCount,
});

const WebDevDispatches = {
};

const WebDev = connect(
  WebDevStates,
  WebDevDispatches,
)(BareWebDev);

export {
  BasePage, Intro, Management, Others, Security, WebDev,
};
