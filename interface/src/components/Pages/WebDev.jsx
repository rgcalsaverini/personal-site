import React from 'react';
import PropTypes from 'prop-types';
import ContentCard from 'components/ContentCard';
import { tWord } from 'app_i18n';
import { t } from 'app_i18n';
import BasePage from '.';
import { CardsContainer, PaddedContainer } from './styles';

const WebDev = (props) => {
  const { reachedSections } = props;
  return (
    <BasePage title="WebDev">
      <PaddedContainer>
        Lorem ipsum dolor sit amet, porro pericula posidonium quo ea, et esse inimicus posidonium eum. Ex quo vidisse numquam, eu quo purto sadipscing. Ad illum persequeris has, an eius audiam discere sea. Ad vim atqui qualisque constituto. Eum diam legere appareat ea, quot consectetuer pro at.
      </PaddedContainer>
      <CardsContainer>
        <ContentCard
          title="Implicit Association Testing"
          articleId="implicit-association-testing"
          tags={['web-app', 'research']}
          contentType={tWord(t('tags.research'))}
          description={t('webDev.iatDescription')}
          image="/imgs/cards/tum.jpg"
          loaded={reachedSections.size > 0}
          actions={[
            [t('gl.seeSourceCode'), false],
            [t('gl.seePage'), false],
          ]}
        />
        <ContentCard
          title={t('webDev.personalPage')}
          articleId="personal-page"
          tags={['react', 'docker', 'kubernetes']}
          description={t('webDev.personalPageDescription')}
          image="/imgs/cards/personal.jpg"
          loaded={reachedSections.size > 0}
          actions={[
            [t('gl.seeSourceCode'), false],
          ]}
        />
      </CardsContainer>
    </BasePage>
  );
};

WebDev.propTypes = {
  reachedSections: PropTypes.instanceOf(Set).isRequired,
};

WebDev.defaultProps = {
};

export default WebDev;
