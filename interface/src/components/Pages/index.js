import {
  BasePage, Intro, Management, Others, Security, WebDev,
} from './connect';

export default BasePage;

export {
  Intro, Management, Others, Security, WebDev,
};
