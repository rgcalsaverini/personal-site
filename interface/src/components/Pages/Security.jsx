import React from 'react';
import PropTypes from 'prop-types';
import ContentCard from 'components/ContentCard';
import { t } from 'app_i18n';
import { CardsContainer, PaddedContainer } from './styles';
import BasePage from '.';


const WebDev = (props) => {
  const { reachedSections } = props;
  return (
    <BasePage title="Security">
      <PaddedContainer>
        Lorem ipsum dolor sit amet, porro pericula posidonium quo ea, et esse inimicus posidonium eum. Ex quo vidisse numquam, eu quo purto sadipscing. Ad illum persequeris has, an eius audiam discere sea. Ad vim atqui qualisque constituto. Eum diam legere appareat ea, quot consectetuer pro at.
      </PaddedContainer>
      <CardsContainer>
        <ContentCard
          title={t('sec.flaskSession')}
          articleId="flask-session-security"
          description={t('sec.flaskSessionDescription')}
          image="/imgs/cards/placeholder.jpg"
          loaded={reachedSections.size > 0}
          tags={['python', 'chrome-extension', 'criptography']}
          actions={[
            [t('gl.tryLive'), false],
            [t('gl.readArticle'), false],
          ]}
        />
        <ContentCard
          title={t('sec.motherload')}
          articleId="reversing-an-old-game"
          description={t('sec.motherloadDescription')}
          image="/imgs/cards/motherload.jpg"
          loaded={reachedSections.size > 0}
          tags={['reverse-engineering', 'http', 'game']}
          actions={[
            [t('gl.readArticle'), false],
          ]}
        />
        <ContentCard
          title={t('sec.sdrCar')}
          articleId="control-rc-car-with-computer"
          description={t('sec.sdrCarDescription')}
          image="/imgs/cards/sdr_car.jpg"
          loaded={reachedSections.size > 0}
          tags={['C++', 'radio-security', 'reverse-engineering', 'hardware']}
          actions={[
            [t('gl.readArticle'), false],
            [t('gl.seeSourceCode'), false],
          ]}
        />
        <ContentCard
          title={t('sec.honeyPort')}
          articleId="honey-pot"
          description={t('sec.honeyPortDescription')}
          tags={['python', 'defense']}
          image="/imgs/cards/placeholder.jpg"
          loaded={reachedSections.size > 0}
          actions={[
            [t('gl.readArticle'), false],
            [t('gl.seeSourceCode'), false],
          ]}
        />
      </CardsContainer>
    </BasePage>
  );
};

WebDev.propTypes = {
  reachedSections: PropTypes.instanceOf(Set).isRequired,
};

WebDev.defaultProps = {
};

export default WebDev;
