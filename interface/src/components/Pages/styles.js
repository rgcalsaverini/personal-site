import styled from 'styled-components';
import {
  flex,
  fonts,
  palette,
  spacing,
  transition,
  onMobile,
  onDesktop,
} from 'theme';

export const PageContainer = styled.div`
  ${flex({ direction: 'column' })}
  ${onMobile`width: 100%;`}
  ${onDesktop`min-height: 100vh;`}
  padding: ${spacing.normal}px;
  position: relative;
`;

export const PaddedContainer = styled.div`
  padding: ${spacing.large}px ${spacing.normal}px;
`;

export const Title = styled.div`
  color: ${palette.darkText};
  font-family: ${fonts.special};
  font-size: 32px;
  align-self: flex-start;
  justify-self: flex-start;
`;

export const LargeDynText = styled.div`
  color: ${palette.darkText};
  font-size: 3vh;
`;

export const IntroContainer = styled.div`
  ${flex({ align: 'center', justify: 'center' })}
  min-height: 50vh;
  ${onDesktop`
    height: calc(100vh - ${spacing.normal * 2}px);
    padding: ${spacing.large}px;
  `}
`;

export const SpaceBlock = styled.div`
  height: ${spacing.large}px;
  width: 1px;
`;

export const FixedRight = styled.div`
  ${flex({ direction: 'column', align: 'center', justify: 'center' })}
  ${onMobile`display: none;`}
  bottom: ${spacing.medium}px;
  font-size: 18px;
  opacity: ${props => (props.fade ? 0 : 1)};
  position: absolute;
  right: ${spacing.medium}px;
  transition: ${transition()};
`;

export const CardsContainer = styled.div`
  ${flex({ direction: 'row' })}
  flex-wrap: wrap;
`;
