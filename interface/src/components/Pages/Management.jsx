import React from 'react';
import PropTypes from 'prop-types';
import ContentCard from 'components/ContentCard';
import { t } from 'app_i18n';
import { CardsContainer, PaddedContainer } from './styles';
import BasePage from '.';

const Management = (props) => {
  const { reachedSections } = props;
  return (
    <BasePage title="Management">
      <PaddedContainer>
        Lorem ipsum dolor sit amet, porro pericula posidonium quo ea, et esse inimicus posidonium eum. Ex quo vidisse numquam, eu quo purto sadipscing. Ad illum persequeris has, an eius audiam discere sea. Ad vim atqui qualisque constituto. Eum diam legere appareat ea, quot consectetuer pro at.
      </PaddedContainer>

      <CardsContainer>
        <ContentCard
          title={t('management.thesis')}
          articleId="thesis"
          description={t('management.thesisDescription')}
          image="/imgs/cards/placeholder.jpg"
          loaded={reachedSections.size > 0}
          actions={[
            [t('gl.readArticle'), false],
          ]}
        />
      </CardsContainer>
    </BasePage>
  );
};

Management.propTypes = {
  reachedSections: PropTypes.instanceOf(Set).isRequired,
};

Management.defaultProps = {
};

export default Management;
