import React from 'react';
import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { t, tWord } from 'app_i18n';
import BasePage from '.';
import {
  LargeDynText, SpaceBlock, IntroContainer, FixedRight,
} from './styles';


const Intro = (props) => {
  const {
    collapsed,
    mobile,
    navigateToSection,
  } = props;

  return (
    <BasePage>
      <IntroContainer>
        <LargeDynText>
          { `${tWord(t('gl.hey'))},`}
          <SpaceBlock />
          {t('text.aboutMe1')}
          <SpaceBlock />
          {t('text.aboutMe2')}
        </LargeDynText>
      </IntroContainer>

      {!mobile && (
      <FixedRight fade={collapsed}>
        {`${tWord(t('gl.scrollDown'))}!`}
        <IconButton
          name={t('gl.scrollDown')}
          size="large"
          onClick={() => navigateToSection('WebDev')}
        >
          <Icon fontSize="large"> arrow_downward </Icon>
        </IconButton>
      </FixedRight>
      ) }
    </BasePage>
  );
};

Intro.propTypes = {
  navigateToSection: PropTypes.func.isRequired,
  collapsed: PropTypes.bool.isRequired,
};

Intro.defaultProps = {
};

export default Intro;
