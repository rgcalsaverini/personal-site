import React from 'react';
import PropTypes from 'prop-types';
import { t } from 'app_i18n';
import { PageContainer, Title } from './styles';

const BasePage = (props) => {
  const {
    mobile, title, children, sections, addSection,
  } = props;

  return (
    <PageContainer
      mobile={mobile}
      ref={(ref) => {
        if (ref && title && ref.offsetTop !== sections.get(title)) {
          addSection(title, ref.offsetTop);
        }
      }}
    >
      { title && (
        <Title>
          {t(`menu.${title}`)}
        </Title>
      )}
      {children}
    </PageContainer>
  );
};

BasePage.propTypes = {
  addSection: PropTypes.func.isRequired,
  title: PropTypes.string,
  sections: PropTypes.object,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]),
};

BasePage.defaultProps = {
  children: [],
  title: null,
  sections: {},
};

export default BasePage;
