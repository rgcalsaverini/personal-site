import React from 'react';
import PropTypes from 'prop-types';
import ContentCard from 'components/ContentCard';
import { t } from 'app_i18n';import { CardsContainer, PaddedContainer } from './styles';
import BasePage from '.';


const Others = (props) => {
  const { reachedSections } = props;
  return (
    <BasePage title="Others">
      <PaddedContainer>
        Lorem ipsum dolor sit amet, porro pericula posidonium quo ea, et esse inimicus posidonium eum. Ex quo vidisse numquam, eu quo purto sadipscing. Ad illum persequeris has, an eius audiam discere sea. Ad vim atqui qualisque constituto. Eum diam legere appareat ea, quot consectetuer pro at.
      </PaddedContainer>
      <CardsContainer>
        <ContentCard
          title={t('other.autoCastle')}
          articleId="automating-game-with-computer-vision"
          description={t('other.autoCastleDescription')}
          tags={['computer-vision', 'automation', 'game']}
          image="/imgs/cards/placeholder.jpg"
          loaded={reachedSections.size > 0}
          actions={[
            [t('gl.seeSourceCode'), false],
            [t('gl.readArticle'), false],
          ]}
        />
        <ContentCard
          title={t('other.3dPrinting')}
          articleId="3d-printing-a-robotic-arm"
          description={t('other.3dPrintingDescription')}
          image="/imgs/cards/placeholder.jpg"
          loaded={reachedSections.size > 0}
          tags={['hardware', 'arduino']}
          actions={[
            [t('gl.seeSourceCode'), false],
            [t('gl.readArticle'), false],
          ]}
        />
      </CardsContainer>
    </BasePage>
  );
};

Others.propTypes = {
  reachedSections: PropTypes.instanceOf(Set).isRequired,
};

Others.defaultProps = {
};

export default Others;
