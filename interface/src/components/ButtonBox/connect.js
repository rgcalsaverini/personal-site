import { connect } from 'react-redux';
import { setDrawer } from 'state_management/actions/side_menu';
import ButtonBox from './ButtonBox';


const stateToProps = state => ({
  mobile: state.sideMenuReducer.mobile,
  collapsed: state.sideMenuReducer.collapsed,
  extraButtons: state.sideMenuReducer.extraButtons,
  langUpdateCount: state.i18nReducer.langUpdateCount,
});

const dispatchToProps = {
  setDrawer,
};

const ConnectedComponent = connect(
  stateToProps,
  dispatchToProps,
)(ButtonBox);

export default ConnectedComponent;
