import React, { Component } from 'react';
import PropTypes from 'prop-types';
import RouterPropTypes from 'react-router-prop-types';
import { withRouter } from 'react-router-dom';
import { htmlBool, locUrl } from 'app_utils';
import Icon from '@material-ui/core/Icon';
import LanguageSelector from 'components/LanguageSelector';
import { t } from 'app_i18n';

import {
  Container,
  MenuButton,
  IconMenuButton,
  LanguageWrapper,
} from './styles';

class ButtonBox extends Component {
  static propTypes = {
    collapsed: PropTypes.bool.isRequired,
    extraButtons: PropTypes.number.isRequired,
    history: RouterPropTypes.history.isRequired,
    mobile: PropTypes.bool.isRequired,
    setDrawer: PropTypes.func.isRequired,
  };

  buttonBlog() {
    const {
      collapsed,
      history,
    } = this.props;

    return (
      <MenuButton
        color="primary"
        collapsed={htmlBool(collapsed)}
        onClick={() => history.push(locUrl('/coming-soon'))}
        name={t('menu.Blog')}
      >
        {t('menu.Blog')}
        <Icon> description </Icon>
      </MenuButton>
    );
  }

  buttonContact() {
    const { collapsed, history } = this.props;
    return (
      <MenuButton
        onClick={() => history.push(locUrl('/contact'))}
        collapsed={htmlBool(collapsed)}
        name={t('menu.Contact')}
      >
        {t('menu.Contact')}
      </MenuButton>
    );
  }

  buttonCurriculum() {
    const { collapsed, history } = this.props;

    return (
      <MenuButton
        color="primary"
        onClick={() => history.push(locUrl('/coming-soon'))}
        collapsed={htmlBool(collapsed)}
        name={t('menu.Resume')}
      >
        {t('menu.Resume')}
        <Icon> play_for_work </Icon>
      </MenuButton>
    );
  }

  buttonGitHub() {
    const { collapsed, history } = this.props;
    return (
      <MenuButton
        onClick={() => history.push(locUrl('/coming-soon'))}
        collapsed={htmlBool(collapsed)}
        name="GitHub"
      >
        GitHub
      </MenuButton>
    );
  }

  buttonLinkedIn() {
    const { collapsed, history } = this.props;
    return (
      <MenuButton
        onClick={() => history.push(locUrl('/coming-soon'))}
        collapsed={htmlBool(collapsed)}
        name="LinkedIn"
      >
        LinkedIn
      </MenuButton>
    );
  }

  buttonMenu() {
    const { setDrawer, collapsed } = this.props;

    return (
      <IconMenuButton
        onClick={() => setDrawer(true)}
        collapsed={htmlBool(collapsed)}
        name="menu"
      >
        <Icon size="large"> menu </Icon>
      </IconMenuButton>
    );
  }

  render() {
    const { collapsed, extraButtons, mobile } = this.props;

    return (
      <Container collapsed={collapsed}>
        {mobile && this.buttonMenu()}
        {this.buttonContact()}
        {extraButtons > 2 && this.buttonGitHub()}
        {extraButtons > 3 && this.buttonLinkedIn()}
        {extraButtons > 1 && this.buttonBlog()}
        {extraButtons > 0 && this.buttonCurriculum()}
        {mobile && (
          <LanguageWrapper>
            <LanguageSelector />
          </LanguageWrapper>
        )}
      </Container>
    );
  }
}

export default withRouter(ButtonBox);

// href="tel:+4915206329527"
