import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import constants from 'app_constants';


import {
  flex,
  spacing,
  palette,
  onMobile,
  onDesktop,
} from 'theme';

export const Container = styled.div`
  ${flex({
    direction: 'row',
    align: 'center',
  })}
  overflow: hidden;
  width: 100%;
  flex-wrap: wrap;

  ${onMobile`
    justify-content: space-between;
    height: ${constants.sideMenuMHeight}px;
    min-height: ${constants.sideMenuMHeight}px;
  `}
  ${onDesktop`
    justify-content: center;
    ${({ collapsed }) => `
      height:  ${collapsed ? '0px' : 'auto'};
      padding-top: ${collapsed ? '0px' : spacing.normal}px;
      padding-bottom: ${collapsed ? '0px' : spacing.normal}px;
    `}
  `}
`;

export const MenuButton = styled(Button)`
  ${onMobile`
    ${({ collapsed, color }) => !color && (collapsed ? (`
      color: ${palette.lightText} !important;
    `) : (`
      color: ${palette.darkText} !important;
    `))}
  `}
  ${onDesktop`
    ${({ color }) => !color && `
      color: ${palette.darkText} !important;
    `}
  `}
`;

export const IconMenuButton = styled(IconButton)`
  color: ${palette.darkText} !important;
  ${onMobile`
    color: ${({ collapsed }) => (collapsed ? palette.lightText : palette.darkText)} !important;
  `}
`;

export const LanguageWrapper = styled.div`
  ${onDesktop`display: none;`}
`;

//
