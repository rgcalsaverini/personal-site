import i18n from 'i18next';
import xhrBbackend from 'i18next-xhr-backend';
import { languageChanged, updateLanguage } from 'state_management/actions/i18n';
import constants, { locales } from 'app_constants';
import { restoreI18nResources } from 'preload';

const restoredRes = restoreI18nResources();

export const t = (key, ...args) => {
  if (i18n.isInitialized) {
    return i18n.t(key, ...args);
  }
  const keyParts = key.split('.');
  let value = restoredRes;
  keyParts.forEach((k) => {
    if (value !== null && typeof value !== 'string') {
      value = value[k] || null;
    }
  });
  return (typeof value === 'string' ? value : key);
};

export default (store) => {
  const selectedLanguage = store.getState().i18nReducer.selected;

  const options = {
    debug: constants.isDev,
    // saveMissing: constants.isDev,

    fallbackLng: 'en',
    whitelist: Object.keys(locales),
    preload: selectedLanguage ? [selectedLanguage] : false,

    ns: ['common'],
    defaultNS: 'common',
    keySeparator: '.',
    interpolation: {
      escapeValue: false,
    },

    backend: {
      loadPath: '/locales/{{lng}}/{{ns}}.json',
      addPath: 'ui-api/i18n-missing/{{lng}}/{{ns}}',
      allowMultiLoading: true,
      crossDomain: false,
      withCredentials: false,
    },
  };

  i18n.use(xhrBbackend);
  i18n.on('languageChanged', target => store.dispatch(languageChanged(target)));

  i18n.init(options,
    () => {
      store.dispatch(updateLanguage(i18n.language));
    });
  return i18n;
};

export const tWord = word => word.charAt(0).toUpperCase() + word.slice(1);
