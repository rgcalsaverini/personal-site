import React from 'react';
import { render, hydrate } from 'react-dom';
import { Provider } from 'react-redux';
import JssProvider from 'react-jss/lib/JssProvider';

import {
  Switch,
  Route,
  BrowserRouter as Router,
  Redirect,
} from 'react-router-dom';
import createStore from 'state_management/store';
import App from 'components/App';
import initI18n from 'app_i18n';
import { MUITheme, GlobalStyle } from 'theme';
import { MuiThemeProvider } from '@material-ui/core/styles';

const store = createStore();
window.i18n = initI18n(store);

const createGenerateClassName = () => {
  let counter = 0;
  const salt = Math.random().toString(36).substring(2, 8);
  return (rule) => {
    const className = `mui--${salt}-${rule.key}-${counter}`;
    counter += 1;
    return className;
  };
};

const RootComponent = () => (
  <div>
    <GlobalStyle />
    <Router>
      <Provider store={store}>
        <JssProvider generateClassName={createGenerateClassName()}>
          <MuiThemeProvider theme={MUITheme}>
            <Switch>
              <Route path="/:loc" component={App} />
              <Redirect to="/en" />
            </Switch>
          </MuiThemeProvider>
        </JssProvider>
      </Provider>
    </Router>
  </div>
);

export default function () {
  const rootElement = document.getElementById('root');

  if (rootElement.hasChildNodes()) {
    hydrate(<RootComponent />, rootElement);
  } else {
    render(<RootComponent />, rootElement);
  }
}
