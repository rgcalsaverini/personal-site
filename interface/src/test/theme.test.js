import { transition, durations, transitionCurve } from 'theme';

test('empty transition to be the default', () => {
  const defaultTrans = `all ${transitionCurve} ${durations.normal}ms`;
  expect(transition()).toBe(defaultTrans);
});

test('string transition', () => {
  const trans = `opacity ${transitionCurve} ${durations.normal}ms, height ${transitionCurve} ${durations.normal}ms`;
  expect(transition('opacity', 'height')).toBe(trans);
});

test('array transition', () => {
  const defaultTrans = `all ${transitionCurve} ${durations.slow}ms, width ${transitionCurve} ${durations.faster}ms`;
  expect(transition(['all', 'slow'], ['width', 'faster'])).toBe(defaultTrans);
});

test('mixed transition', () => {
  const defaultTrans = `all ${transitionCurve} ${durations.normal}ms, border-radius ${transitionCurve} ${durations.fast}ms`;
  expect(transition('all', ['border-radius', 'fast'])).toBe(defaultTrans);
});
