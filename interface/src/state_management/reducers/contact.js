const getError = action => (
  (action.error
    && action.error.response
    && action.error.response.data
    && action.error.response.data.error) || {}
);

const reducer = (state = {
  email: undefined,
  emailError: false,
  message: undefined,
  messageError: false,
  messageSent: false,
  pendingResponse: false,
}, action) => {
  switch (action.type) {
    case 'SET_MESSAGE': {
      return {
        ...state,
        message: action.message,
        messageError: false,
      };
    }
    case 'SET_EMAIL': {
      return {
        ...state,
        email: action.email,
        emailError: false,
      };
    }
    case 'SEND_MESSAGE': {
      return {
        ...state,
        pendingResponse: true,
      };
    }
    case 'SEND_MESSAGE_FAIL': {
      return {
        ...state,
        emailError: getError(action).t_email || false,
        messageError: getError(action).t_message || false,
        pendingResponse: false,
      };
    }
    case 'SEND_MESSAGE_SUCCESS': {
      return {
        ...state,
        emailError: false,
        message: undefined,
        messageError: false,
        messageSent: true,
        pendingResponse: false,
      };
    }
    case 'SEND_ANOTHER_MESSAGE': {
      return {
        ...state,
        messageSent: false,
      };
    }
    case 'CLEAR_CONTACT_DIALOG': {
      return {
        ...state,
        email: undefined,
        message: undefined,
        emailError: false,
        messageError: false,
        messageSent: false,
        pendingResponse: false,
      };
    }
    default:
      return state;
  }
};

export default reducer;
