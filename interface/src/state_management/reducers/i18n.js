const reducer = (state = {
  selected: null,
  dialogOpen: false,
  langUpdateCount: 0,
}, action) => {
  switch (action.type) {
    case 'I18N_UPDATE_LANGUAGE': {
      return {
        ...state,
        selected: action.language,
        dialogOpen: false,
        langUpdateCount: state.langUpdateCount + 1,
      };
    }
    case 'SET_I18N_DIALOG': {
      return {
        ...state,
        dialogOpen: action.open === true,
      };
    }
    default:
      return state;
  }
};

export default reducer;
