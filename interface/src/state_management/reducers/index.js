import { combineReducers } from 'redux';

import sideMenuReducer from './side_menu';
import i18nReducer from './i18n';
import contactReducer from './contact';

export default combineReducers({
  sideMenuReducer,
  i18nReducer,
  contactReducer,
});
