
const reducer = (state = {
  collapsed: false,
  drawerOpen: false,
  extraButtons: 0,
  mobile: false,
  sections: new Map(),
  sectionOffsets: new Map(),
  activeSection: null,
  reachedSections: new Set(),
  numCards: 0,
}, action) => {
  switch (action.type) {
    case 'UPDATE_MOBILE':
      return {
        ...state,
        mobile: action.isMobile,
        drawerOpen: state.drawerOpen && action.isMobile,
      };
    case 'UPDATE_EXTRA_BUTTONS':
      return {
        ...state,
        extraButtons: action.extraButtons,
      };
    case 'UPDATE_NUM_CARDS':
      return {
        ...state,
        numCards: action.numCards,
      };
    case 'SET_MOBILE_DRAWER':
      return {
        ...state,
        drawerOpen: action.open,
      };
    case 'SET_SM_COLLAPSED':
      return {
        ...state,
        collapsed: action.collapsed,
      };
    case 'ADD_SECTION': {
      const sections = new Map([...state.sections, [action.title, action.offset]]);
      return {
        ...state,
        sections,
        sectionOffsets: new Map(Array.from(sections)
          .map(([title, offset]) => [offset, title])
          .sort((lh, rh) => lh[0] - rh[0])),
      };
    }
    case 'SET_ACTIVE_SECTION':
      return {
        ...state,
        activeSection: action.title,
        reachedSections: new Set([...Array.from(state.reachedSections), action.title]),
      };
    default:
      return state;
  }
};

export default reducer;
