import { mockStore, getActionsOfType, getActionTypes } from './utils';

const last_updated = {
  unix: 1548354583.252268,
  iso: '2019-01-24T18:29:43.252268',
  tuple: [2019, 1, 24, 18, 29, 43],
  with_time: true,
  tz: null,
};

jest.mock('i18next', () => {
  const mock = require('./mock_i18next');
  return mock;
});

test('correclty load locales', () => {
  const store = mockStore();

  const data = {
    available: { loc1: 'Locale 1', loc2: 'Locale 2' },
    locale: {
      last_updated,
      selected: 'loc1',
    },
  };

  store.dispatch({
    type: '_GET_I18N_SUCCESS',
    payload: {
      data,
    },
  });
  store.whenComplete(() => {
    const state = store.getState().i18nReducer;
    expect(getActionTypes(store)).toEqual(['_GET_I18N_SUCCESS', 'SET_I18N_CONFIGS']);
    expect(Object.keys(state.locales)).toEqual(Object.keys(data.available));
    expect(Object.values(state.locales)).toEqual(Object.values(data.available));
    expect(state.selected).toEqual(data.locale.selected);
  });
});
