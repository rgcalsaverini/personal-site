class MockI18next {
  whitelist = {}

  language = null;

  changeLanguage(loc) {
    this.language = loc;
  }
}

const mock = new MockI18next();

export default mock;
