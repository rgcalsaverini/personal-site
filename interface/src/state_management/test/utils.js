import { createMockStore } from 'redux-logic-test';
import { middlewares } from 'state_management/store';
import allLogic from 'state_management/logic';
import allReducers from 'state_management/reducers';

export const getActionsOfType = (store, type) => store.actions.filter(a => a.type === type);
export const getActionTypes = store => store.actions.map(a => a.type);

export const mockStore = () => {
  const { logic, axios, ...validMiddlewares } = middlewares;
  return createMockStore({
    reducer: allReducers,
    logic: allLogic,
    middleware: Object.values(validMiddlewares),
  });
};
