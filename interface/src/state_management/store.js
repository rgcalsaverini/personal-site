import { applyMiddleware, createStore } from 'redux';
import { createLogicMiddleware } from 'redux-logic';
import axios from 'axios';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import axiosMiddleware from 'redux-axios-middleware';
import immutable from 'redux-immutable-state-invariant';
import logic from 'state_management/logic';
import reducer from 'state_management/reducers';
import { resizeWindow, rightScrolled } from 'state_management/actions/side_menu';
import createDebounce from 'redux-debounced';
import constants from 'app_constants';
import _ from 'lodash';
import { restoreState, snapSave } from 'preload';

const axiosClient = axios.create({
  responseType: 'json',
});

const axiosMiddlewareConfig = {
};

export const middlewares = {
  debounce: createDebounce(),
  axios: axiosMiddleware(axiosClient, axiosMiddlewareConfig),
  logic: createLogicMiddleware(logic),
  thunk,
};

if (constants.isDev) {
  middlewares.logger = createLogger();
  middlewares.immutable = immutable();
}

export default () => {
  const initialState = restoreState();
  const store = createStore(reducer,
    initialState,
    applyMiddleware(...Object.values(middlewares)));
  if (constants.isDev) {
    window.store = store;
  }

  window.onresize = _.throttle(() => store.dispatch(resizeWindow()), 100);
  window.onscroll = _.throttle(() => store.dispatch(rightScrolled()), 100);
  window.onload = () => store.dispatch(resizeWindow());
  if (navigator.userAgent === 'ReactSnap') {
    window.snapSaveState = () => snapSave(store);
  }
  store.dispatch(resizeWindow());

  return store;
};
