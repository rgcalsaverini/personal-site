import { createLogic } from 'redux-logic';
import constants from 'app_constants';
import { getWidth } from 'app_utils';
import { animateScroll } from 'react-scroll';
import { spacing } from 'theme';
import { setDrawer } from 'state_management/actions/side_menu';

const baseConfig = {
  latest: true,
};

const getBodyScroll = () => {
  const offset = (window.pageYOffset || document.scrollTop);
  const top = (document.clientTop || 0);
  const scroll = offset - top;
  return Number.isNaN(scroll) ? 0 : scroll;
};

const checkIfIsMobile = createLogic({
  ...baseConfig,
  type: '_WINDOW_RESIZED',
  process({ getState }, dispatch, done) {
    const width = getWidth();
    const isMobile = width < constants.mobileWidth;
    const extraButtons = Math.round((width - 320) / 100);
    const state = getState().sideMenuReducer;
    const numCards = Math.floor(width / (480 + 2 * spacing.normal)) || 1;

    if (isMobile !== state.mobile) {
      dispatch({ type: 'UPDATE_MOBILE', isMobile });
    }

    if (extraButtons !== state.extraButtons) {
      dispatch({ type: 'UPDATE_EXTRA_BUTTONS', extraButtons });
    }

    if (numCards !== state.numCards) {
      dispatch({ type: 'UPDATE_NUM_CARDS', numCards });
    }

    done();
  },
});

const toggleDrawer = createLogic({
  ...baseConfig,
  type: '_SET_DRAWER',
  process({ getState, action }, dispatch, done) {
    const state = getState();
    if (state.sideMenuReducer.mobile) {
      dispatch({
        type: 'SET_MOBILE_DRAWER',
        open: typeof action.open === 'boolean' ? action.open : !state.sideMenuReducer.drawerOpen,
      });
    }
    done();
  },
});

const rightCellScrolled = createLogic({
  ...baseConfig,
  type: '_RIGHT_SCROLLED',
  process({ getState }, dispatch, done) {
    const {
      activeSection,
      collapsed,
      mobile,
      sectionOffsets,
    } = getState().sideMenuReducer;
    const offset = getBodyScroll();
    const collapseThreshold = window.innerHeight * constants.collapseThreshold;
    let shouldCollapse;

    if (mobile && !collapsed) {
      shouldCollapse = offset > collapseThreshold;
    } else if (mobile && collapsed) {
      shouldCollapse = offset >= constants.appBarHeight;
    } else {
      shouldCollapse = offset > collapseThreshold;
    }

    if (!collapsed && shouldCollapse) {
      dispatch({ type: 'SET_SM_COLLAPSED', collapsed: true });
    } else if (collapsed && !shouldCollapse) {
      dispatch({ type: 'SET_SM_COLLAPSED', collapsed: false });
    }

    const secIter = sectionOffsets.entries();
    const secThreshold = document.documentElement.clientHeight * constants.secThreshold;
    let activeTitle = null;

    while (true) {
      const entry = secIter.next().value;

      if (!entry) {
        break;
      }
      const [secOffset, title] = entry;
      if (secOffset + secThreshold >= offset) {
        activeTitle = title;
        break;
      }
    }

    if (activeSection !== activeTitle) {
      dispatch({ type: 'SET_ACTIVE_SECTION', title: activeTitle });
    }

    done();
  },
});

const navigateToSection = createLogic({
  ...baseConfig,
  type: '_NAVIGATE_TO_SECTION',
  process({ action, getState }, dispatch, done) {
    const { sections } = getState().sideMenuReducer;
    const offset = sections.get(action.title) || action.title;
    if (Number.isInteger(offset)) {
      animateScroll.scrollTo(offset);
    }
    dispatch(setDrawer(false));
    done();
  },
});


export default [
  checkIfIsMobile,
  toggleDrawer,
  rightCellScrolled,
  navigateToSection,
];
