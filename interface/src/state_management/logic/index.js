import sideMenuLogic from './side_menu';
import i18nLogic from './i18n';

export default [
  ...sideMenuLogic,
  ...i18nLogic,
];
