import { createLogic } from 'redux-logic';
import { setDialogOpen, updateLanguage } from 'state_management/actions/i18n';

export const onLanguageChanged = createLogic({
  type: ['_I18N_LANGUAGE_CHANGED'],
  latest: true,
  process({ action, getState }, dispatch, done) {
    const { selected } = getState().i18nReducer;
    const target = action.language;
    if (target && target !== selected) {
      dispatch(updateLanguage(action.language));
    }
    dispatch(setDialogOpen(false));
    done();
  },
});

export default [
  onLanguageChanged,
];
