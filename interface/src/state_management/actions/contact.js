export const sendMessage = (email, message) => (
  {
    payload: {
      request: {
        method: 'post',
        data: { email, message },
        url: '/ui-api/messages',
      },
    },
    type: 'SEND_MESSAGE',
  }
);
export const sendAnotherMessage = () => (
  {
    type: 'SEND_ANOTHER_MESSAGE',
  }
);

export const setMessage = message => (
  {
    type: 'SET_MESSAGE',
    message,
  }
);

export const setEmail = email => (
  {
    type: 'SET_EMAIL',
    email,
  }
);

export const clearContactDialog = () => (
  {
    type: 'CLEAR_CONTACT_DIALOG',
  }
);
