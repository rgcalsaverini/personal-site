export const resizeWindow = () => (
  {
    type: '_WINDOW_RESIZED',
  }
);

export const setDrawer = open => (
  {
    type: '_SET_DRAWER',
    open,
  }
);

export const setCollapsed = collapsed => (
  {
    type: 'SET_SM_COLLAPSED',
    collapsed,
  }
);

export const rightScrolled = () => (
  {
    type: '_RIGHT_SCROLLED',
  }
);

export const addSection = (title, offset) => (
  {
    type: 'ADD_SECTION',
    offset,
    title,
  }
);

export const navigateToSection = title => (
  {
    type: '_NAVIGATE_TO_SECTION',
    title,
  }
);
