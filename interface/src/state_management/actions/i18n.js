export const setDialogOpen = open => (
  {
    type: 'SET_I18N_DIALOG',
    open,
  }
);

export const languageChanged = language => (
  {
    type: '_I18N_LANGUAGE_CHANGED',
    language,
  }
);

export const updateLanguage = language => (
  {
    type: 'I18N_UPDATE_LANGUAGE',
    language,
  }
);
