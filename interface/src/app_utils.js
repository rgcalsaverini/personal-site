import i18n from 'i18next';
import constants from 'app_constants';

export const getWidth = () => Math.max(
  document.documentElement.clientWidth,
  document.body.scrollWidth,
  document.documentElement.scrollWidth,
  document.body.offsetWidth,
  document.documentElement.offsetWidth,
);

export const getHeight = () => Math.max(
  document.documentElement.clientWidth,
  document.body.scrollWidth,
  document.documentElement.scrollWidth,
  document.body.offsetWidth,
  document.documentElement.offsetWidth,
);

export const getAspectRatio = () => parseFloat(getWidth()) / parseFloat(getHeight());

export const htmlBool = (value => (value ? 'true' : undefined));

export const locUrl = (path, full, overrideLoc) => {
  const loc = overrideLoc || i18n.language;
  const locPath = `/${loc}/${(path || '').replace(/^\/+|\/+$/g, '')}`.replace(/\/+$/g, '');
  if (full) {
    return `${constants.host.replace(/\/+$/g, '')}${locPath}`;
  }
  return locPath;
};


export const apiHost = navigator.userAgent === 'ReactSnap' ? 'http://localhost:5000' : '';
