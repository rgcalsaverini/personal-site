import i18n from 'i18next';

const serializeReducer = (stateReducer) => {
  const serializable = {};
  const changedTypes = {};

  Object.keys(stateReducer).forEach((stateKey) => {
    if (stateReducer[stateKey] instanceof Map) {
      serializable[stateKey] = Array.from(stateReducer[stateKey]);
      changedTypes[stateKey] = 'Map';
    } else if (stateReducer[stateKey] instanceof Set) {
      serializable[stateKey] = Array.from(stateReducer[stateKey]);
      changedTypes[stateKey] = 'Set';
    } else {
      serializable[stateKey] = stateReducer[stateKey];
    }
  });

  return [serializable, changedTypes];
};

const serializeState = (state) => {
  const serializable = {};
  const changedTypes = {};
  Object.keys(state).forEach((reducerKey) => {
    const [serReducer, changedReducer] = serializeReducer(state[reducerKey]);
    serializable[reducerKey] = serReducer;
    changedTypes[reducerKey] = changedReducer;
  });
  return [serializable, changedTypes];
};

export const restoreState = () => {
  const [serState, changedTypes] = window.__PRELOADED_STATE__ || [{}, {}];
  delete window.__PRELOADED_STATE__;

  const restored = {};

  Object.keys(serState).forEach((reducerKey) => {
    restored[reducerKey] = serState[reducerKey];
    Object.keys(changedTypes[reducerKey]).forEach((stateKey) => {
      const originalType = changedTypes[reducerKey][stateKey];
      if (originalType === 'Map') {
        restored[reducerKey][stateKey] = new Map(serState[reducerKey][stateKey]);
      } else if (originalType === 'Set') {
        restored[reducerKey][stateKey] = new Set(serState[reducerKey][stateKey]);
      }
    });
  });

  return restored;
};

export const restoreI18nResources = () => window.__I18NRES__ || {};

export const snapSave = store => ({
  __PRELOADED_STATE__: serializeState(store.getState()),
  __I18NRES__: i18n.services.resourceStore.getResourceBundle(i18n.language),
});
