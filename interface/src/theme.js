import { css, createGlobalStyle } from 'styled-components';
import { createMuiTheme } from '@material-ui/core/styles';
import pink from '@material-ui/core/colors/pink';
import red from '@material-ui/core/colors/red';

export const palette = {
  pageBg: 'white',
  accentBg: '#616260',
  lightText: '#E3E3E5',
  darkText: '#413E46',
  accent: '#00B27A',
};

export const spacing = {
  normal: 16,
  medium: 24,
  large: 32,
};
export const onShortMobile = (...args) => css`
  @media (min-aspect-ratio: 100/130) and (max-aspect-ratio: 100/90) {
    ${css(...args)}
  }
`;

export const onMobile = (...args) => css`
  @media (max-aspect-ratio: 100/90) {
    ${css(...args)}
  }
`;
export const onDesktop = (...args) => css`
  @media (min-aspect-ratio: 10001/9000) {
    ${css(...args)}
  }
`;

export const transitionCurve = 'ease-in-out';

export const durations = {
  instant: 0,
  faster: 100,
  fast: 150,
  normal: 200,
  slow: 300,
};

export const transition = (...items) => {
  if (items.length === 0) {
    return `all ${transitionCurve} ${durations.normal}ms`;
  }
  return items.map((step) => {
    const element = typeof step === 'string' ? step : step[0];
    const duration = typeof step === 'string' ? 'normal' : step[1];
    return `${element} ${transitionCurve} ${durations[duration || 'normal']}ms`;
  }).join(', ');
};

export const fonts = {
  normal: "'Roboto', sans-serif",
  special: "'Rubik Mono One', sans-serif",
};

export const GlobalStyle = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Roboto:400,500,700|Rubik+Mono+One');
  @import url('https://fonts.googleapis.com/icon?family=Material+Icons');

  body {
    font-family: ${fonts.normal};
    padding: 0px;
    margin: 0px;
    background-color: ${palette.pageBg}
  }
`;

export const flex = ({ direction, align, justify }) => (css`
  display: flex !important;
  box-sizing: border-box !important;
  ${direction && (direction === 'row' ? 'flex-direction: row !important;' : 'flex-direction: column !important;')}
  ${align && `align-items: ${align} !important;`}
  ${justify && `justify-content: ${justify} !important;`}
`);


export const text = ({
  color, size, weight, family,
}) => (`
  color: ${palette[color] || color || palette.darkText};
  ${size && `font-size: ${size}`}
  ${weight && `font-weight: ${weight}`}
  ${family && `font-family: ${fonts[family]}`}
`);

export const expand = css`
  bottom: 0px;
  left: 0px;
  right: 0px;
  top: 0px;
`;

export const px = v => `${v}px`;

export const MUITheme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
  palette: {
    primary: { 500: palette.accent },
    secondary: pink,
    error: red,
    contrastThreshold: 3, // 2 might be better
    tonalOffset: 0.2,
  },
});
