export default {
  isDev: !process.env.NODE_ENV
    || process.env.NODE_ENV === 'development'
    || document.location.search.includes('debug'),
  mobileWidth: 900,
  secThreshold: 0.4,
  sideMenuWidth: 160,
  collapseThreshold: 0.2,
  appBarHeight: 60,
  host: 'https://rui.calsaverini.com',
};

export const locales = {
  de: 'Deutsch',
  en: 'English',
  'en-PR': 'Pirate english',
  'pt-BR': 'Português (BR)',
};
